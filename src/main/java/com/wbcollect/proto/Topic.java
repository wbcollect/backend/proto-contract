package com.wbcollect.proto;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.MessageOrBuilder;

public final class Topic<C extends GeneratedMessageV3 & MessageOrBuilder> {

    public final String name;
    public final Class<C> clazz;

    private Topic(String name, Class<C> clazz) {
        this.name = name;
        this.clazz = clazz;
    }

    static <C extends GeneratedMessageV3 & MessageOrBuilder> Topic<C> of(String name, Class<C> clazz){
        return new Topic<>(name, clazz);
    }

    public String getName() {
        return name;
    }

    public Class<C> getClazz() {
        return clazz;
    }
}
