package com.wbcollect.proto;

public interface Microservice {

    String DOT = ".";

    String VERSION = "v1";

    String VERSION_ = VERSION + DOT;
}
