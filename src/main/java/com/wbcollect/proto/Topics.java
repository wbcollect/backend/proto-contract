package com.wbcollect.proto;

import com.wbcollect.bot.BotProto;
import com.wbcollect.usermanager.UserManagerProto;

@SuppressWarnings("unused")
public interface Topics {

    interface USER_MANAGER extends Microservice {
        String NAME = "usermanager";

        String USER_CREATED_NAME = VERSION_ + NAME + DOT + "usercreated";

        Class<UserManagerProto.UserCreated> USER_CREATED_CLASS = UserManagerProto.UserCreated.class;

        Topic<UserManagerProto.UserCreated> USER_CREATED = Topic.of(USER_CREATED_NAME, USER_CREATED_CLASS);
    }

    interface BOT extends Microservice {
        String NAME = "bot";

        String INBOUND_TASK_NAME = VERSION_ + NAME + DOT + "inboundtask";
        String OUTBOUND_TASK_NAME = VERSION_ + NAME + DOT + "outboundtask";

        Class<BotProto.InboundTask> INBOUND_TASK_CLASS = BotProto.InboundTask.class;
        Class<BotProto.OutboundTask> OUTBOUND_TASK_CLASS = BotProto.OutboundTask.class;

        Topic<BotProto.InboundTask> INBOUND_TASK = Topic.of(INBOUND_TASK_NAME, INBOUND_TASK_CLASS);
        Topic<BotProto.OutboundTask> OUTBOUND_TASK = Topic.of(OUTBOUND_TASK_NAME, OUTBOUND_TASK_CLASS);
    }

}
