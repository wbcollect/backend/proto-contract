package com.wbcollect.proto.util;

import com.google.protobuf.Message;
import com.google.protobuf.MessageLite;
import com.google.protobuf.Parser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class ProtoUtils {

    public static byte[] toByteArrayDelimitedOrThrow(MessageLite message) {
        try {
            return toByteArrayDelimited(message);
        } catch (IOException e) {
            throw new RuntimeException("Unable to serialize proto", e);
        }
    }

    public static byte[] toByteArrayDelimitedOrNull(MessageLite message) {
        try {
            return toByteArrayDelimited(message);
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] toByteArrayDelimited(MessageLite message) throws IOException {
        try (var stream = new ByteArrayOutputStream()) {
            message.writeDelimitedTo(stream);
            return stream.toByteArray();
        }
    }

    public static <T extends Message> Stream<T> parseStream(Stream<byte[]> buffers, Class<T> messageType) {
        return buffers.map(buffer -> parseOrNull(buffer, messageType))
                .filter(Objects::nonNull);
    }

    public static <T extends Message> Optional<T> parse(byte[] bytes, Class<T> messageType) {
        try {
            return Optional.of(tryParse(bytes, messageType));
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    public static <T extends Message> T parseOrNull(byte[] bytes, Class<T> messageType) {
        return parse(bytes, messageType).orElse(null);
    }

    public static <T extends Message> T parseOrThrow(byte[] bytes, Class<T> messageType) throws RuntimeException {
        return parse(bytes, messageType).orElseThrow(() -> new RuntimeException("unable to parse protobuf message for type (" + messageType.getName() + ")"));
    }

    @SuppressWarnings("unchecked")
    public static <T extends Message> T tryParse(byte[] bytes, Class<T> messageType) throws IOException {
        Method parserMethod;
        try {
            parserMethod = messageType.getMethod("parser");
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("this should never happen, unable to resolve parser method for protobuf message", e);
        }
        Parser<T> messageParser;
        try {
            messageParser = (Parser<T>) parserMethod.invoke(null);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("unable to determine parser for protobuf message", e);
        }
        return tryParse(bytes, messageParser);
    }

    public static <T> T tryParse(byte[] bytes, Parser<T> parser) throws IOException {
        try (var stream = new ByteArrayInputStream(bytes)) {
            return parser.parseDelimitedFrom(stream);
        }
    }
}
